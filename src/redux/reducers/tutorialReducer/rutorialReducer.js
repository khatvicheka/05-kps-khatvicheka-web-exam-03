const defaultState={
    data: []
}

export const tutorialReducer = (state=defaultState, action)=>{
    switch(action.type){
        case GET_TUTORIALS:
            return{
                ...state,
                data: action.data
            }
        default :
            return state
    }

}