import Axios from "axios"

export const getTutorials=()=>{
    
    const innerGetTutorial = async (dispatch)=>{
        const result = await Axios.get("http://110.74.194.125:3535/api/tutorials")
        dispatch({
            type: GET_TUTORIALS,
            data : result.data
        })
    }

    return innerGetTutorial;
}