import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const GetAllTutorials = (props) => (
  <div className="GetAllTutorialsWrapper">
    {props.data.map((data, index) => {
      return <p key={index}>{data.title}</p>;
    })}
  </div>
);

const mapStateToProps = (state) => {
  return {
    data: state.tutorialReducer.data,
  };
};

// const mapDispatchToProps = dispatch => ({
//   // fnBlaBla: () => dispatch(action.name()),
// });

export default connect(mapStateToProps, null)(GetAllTutorials);
